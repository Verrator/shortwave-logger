#!/usr/bin/python
# -*- coding: utf-8 -*-

#SWL Logger by Reece Albright 2018


#initilization

import sqlite3 as lite
import subprocess as shell #for screen clearing
import time #for delays
import bs4 as bs # bs4, requests, lxml are used for grabbing web info for solar propigation
import requests 
import lxml 


db = lite.connect('logger.db')
cursor = db.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS logs(ID INTEGER PRIMARY KEY AUTOINCREMENT, name text, freq text, mode text, utc text, date text, 
contact text, notes text)''') #if the table doesnt exist create it


#solar data

def solar():

	link = "http://www.wm7d.net/hamradio/solar/index.shtml"
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'}
        site_source = requests.get(link, headers = headers)
        data = site_source.text
        soup = bs.BeautifulSoup(data, 'lxml')


	shell.call("clear")
	print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
        print("\n\n\n\033[1;37;40m")
	print("Solar Index Data from http://www.wm7d.net/hamradio/solar/index.shtml")
        messagetext = soup.find_all('font')
	print messagetext[0].get_text()
	print ("SFI: %s" % messagetext[2].get_text())
        print ("A: %s" % messagetext[3].get_text())
        print ("K: %s" % messagetext[4].get_text())
	raw_input("Press enter to continue")
        return


#print logs

def printlogs(rows):

                for row in rows:
                        print("ID: %s" % row[0])
			print("Name: %s" % row[1])
                        print("Freq: %s" % row[2])
                        print("Mode: %s" % row[3])
                        print("UTC: %s" % row[4])
                        print("Date: %s" % row[5])
                        print("Contact: %s" % row[6])
                        print("Notes: %s" % row[7])
	                print(" ")


                raw_input("Press enter to return")
                return

#Add Logs

def addlog():
        shell.call("clear")
        cur = db.cursor()

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
        print("\n\n\n\033[1;37;40m")

	duplicate = 0
	name = raw_input("Name of station: ").upper() #formalize all inputs to uppercase to help reduce duplicates further
	freq = raw_input("Freq: ").upper()
	mode = raw_input("Mode: ").upper()

	if cur.execute("SELECT * FROM logs WHERE name=?", (name, )).fetchone() is None: #doing checks for previous entries
		dupliate = duplicate
	else:
		duplicate = duplicate + 1
	if cur.execute("SELECT * FROM logs WHERE freq=?", (freq, )).fetchone() is  None:
		duplicate = duplicate
	else:
                duplicate = duplicate + 1
        if cur.execute("SELECT count(*) FROM logs WHERE mode=?", (mode, )).fetchone() is  None:
		duplicate = duplicate
	else:
                duplicate = duplicate + 1

	if duplicate >=3:
		print ("Duplicate found: ")
		print (" ")
		cur.execute("SELECT * from logs where name = ?", (name,))
                rows = cur.fetchall()
		printlogs(rows)
#quick
	else:
		utc = raw_input("UTC: ").upper()
		date = raw_input("Date: ").upper()
		contact = raw_input("Contact Info: ").upper()
		notes = raw_input("Notes: ")

		try:
			with db:
				cursor.execute("INSERT INTO logs(name, freq, mode, utc, date, contact, notes)  VALUES (?,?,?,?,?,?,?)", (name, freq, mode, utc, date, contact, notes))
				db.commit()
		finally:
			return

def searchlog():

	shell.call("clear")
       	db = lite.connect('logger.db') #reconnect to DB is it was closed
	cur = db.cursor()

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
       	print("\n\n\n\033[1;37;40m")
	print("1. Name ")
	print("2. Frequency ")
	print("3. Date ")
        print("4. Mode ")
	print("5. Back \n")


        selection = raw_input("1 - 5: ")

        if selection == "1":
		name = raw_input("Name of station: ").upper()
		cur.execute("SELECT * from logs where name = ?", (name,))
		rows = cur.fetchall()
		printlogs(rows)
		return

        elif selection == "2":
                freq = raw_input("Freq: ").upper()
                cur.execute("SELECT * from logs where freq = ?", (freq,))
                rows = cur.fetchall()
                printlogs(rows)
		return

        elif selection == "3":
                date = raw_input("Date: ").upper()
                cur.execute("SELECT * from logs where date = ?", (date,))
                rows = cur.fetchall()
                printlogs(rows)
		return

        elif selection == "4":
                mode = raw_input("Mode: ").upper()
                cur.execute("SELECT * from logs where mode = ?", (mode,))
                rows = cur.fetchall()
                printlogs(rows)
		return

        elif selection == "5":

                return

        else:
                print("Invalid choice, try again")
                time.sleep(2)
                shell.call("clear")


def alllog():

        shell.call("clear")
        db = lite.connect('logger.db') #reconnect to DB is it was closed
        cur = db.cursor()

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
	print("\n\n\033[1;37;40m")

	cur.execute("SELECT * from logs")
        rows = cur.fetchall()
        printlogs(rows)


def deletelog():
	shell.call("clear")
        cur = db.cursor()

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
	print("\n\n\033[1;37;40m")
	
	select = raw_input("What entry number would you like to remove? ")
	try:
		with db:
			
       			cur.execute("DELETE from logs where id = ?", (select,))
			print("Record has been removed")
			db.commit()
			raw_input("Press enter to return")
	finally:
		return
		
def editlog():

        shell.call("clear")
        cur = db.cursor()

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")
        print("\n\n\033[1;37;40m")

	id = raw_input("Which record would you like to edit?: ").upper()        
	cur.execute("SELECT * from logs where id = ?",(id,))
        rows = cur.fetchall()
        printlogs(rows)

	selection = raw_input("\nDo you wish to edit this record still? ").upper()

	if selection == "Y":
		edit = raw_input("Edit which: Name, Freq, Mode, UTC, Date, Contact, or Notes? ").lower()
		
		if edit != "name" and edit != "freq" and edit != "mode" and edit != "utc" and edit != "date" and edit != "contact" and edit != "notes":
			print("Sorry you did not make a proper selection\n")
			raw_input("Press enter to return")
			return
		elif edit == "name": 
			change = raw_input("\nAnd the new value? ").upper()
			cur.execute("UPDATE logs SET name = ? WHERE id = ?", (change, id))
        		cur.execute("SELECT * from logs where id = ?",(id,))
        		rows = cur.fetchall()

	                print("Record has been updated")
        	        db.commit()
                	raw_input("Press enter to return")
                	printlogs(rows)

                elif edit == "freq":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET freq = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
                	printlogs(rows)

                elif edit == "mode":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET mode = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
                	printlogs(rows)

                elif edit == "utc":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET utc = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
                	printlogs(rows)

                elif edit == "date":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET date = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
                	printlogs(rows)

                elif edit == "contact":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET contact = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
               		printlogs(rows)

                elif edit == "notes":
                        change = raw_input("\nAnd the new value? ").upper()
                        cur.execute("UPDATE logs SET notes = ? WHERE id = ?", (change, id))
                        cur.execute("SELECT * from logs where id = ?",(id,))
                        rows = cur.fetchall()

                        print("Record has been updated")
                        db.commit()
                        raw_input("Press enter to return")
                	printlogs(rows)

	else:
		return
		


#Main Loop
def main():

	shell.call("clear")

        print("\033[1;37;40mShortwave Logger by \033[1;31;40mReece Albright \033[1;34;40m2018")	
	print("\n\n\n\033[1;37;40m")
	print("1. Add Log 	7. Quit")
	print("2. Search Logs ")
	print("3. Delete Log ")
	print("4. Show All Logs ")
	print("5. Edit Log")
	print("6. Solar Data\n")
	print(" ")

	selection = raw_input("1 - 6: ")

	if selection == "1":
		addlog()
		return

	elif selection == "2":
		searchlog()
		return

	elif selection == "3":
		deletelog()
		return

	elif selection == "4":
		alllog()
		return

        elif selection == "5":
                editlog()
                return

	elif selection == "6":
		solar()
		return

	elif selection == "7":
		db.close
		exit()
	else:
		print("Invalid choice, try again")
		time.sleep(2)
		shell.call("clear")
while True:
	main()
